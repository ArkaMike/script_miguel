const http = require('http');
//puerto en el que la app correra
const port = process.env.PORT || 5001;
const app = require('./app');
// //need to pass a listneer , a function to handling inc request
const server = http.createServer(app);
server.listen(port, () => console.log(`Express Running on Port: ${port}...`));
